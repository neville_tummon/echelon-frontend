var AppDispatcher = require('../Dispatcher/AppDispatcher');
var DefectsApi = require('../Utils/DefectsApi');
var Constants = require('../Constants/Constants');
var request = require('superagent');
var SessionStore = require('../Stores/SessionStore');

module.exports = {
  defectListRequested: function(pageId, query) {
    var token = SessionStore.getData().authToken;
    var link = "http://localhost:8000/dirts/v1/dirts/"
    
    request.get(link)
      .type('application/json')
      .set('Authorization', 'JWT ' + token)
      .end(function(err, res) {
        console.log(res);
        if (err) {
          console.log("error");
        }
        console.log(JSON.parse(res.text));
      });
    
    AppDispatcher.handleViewAction({
      type: Constants.DEFECTLIST_REQUESTED,
      items: DefectsApi.getIssues(pageId),
      query: query
    });
  },
  defectDetailsRequested: function(id) {
    var defect = DefectsApi.getDetail(id);
    AppDispatcher.handleViewAction({
      type: Constants.DEFECT_SELECTED,
      defect: defect
    });
  }
};
