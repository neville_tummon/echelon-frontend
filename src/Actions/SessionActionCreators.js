var AppDispatcher = require('../Dispatcher/AppDispatcher');
var Constants = require('../Constants/Constants');
var request = require('superagent');

var _dispatchError = function(errors) {
	var payload = {
		type: Constants.AUTH_FAILED,
		errors: errors
	};
	console.log(payload);
	AppDispatcher.handleViewAction(payload);
}

var _dispatchLoginSuccess = function(username, token) {
	var payload = {
		type: Constants.USER_LOGGED_IN,
		user: username,
		token: token
	};
	AppDispatcher.handleViewAction(payload);
}

module.exports = {
  authenticateUser: function(username, password) {
	var link = "http://localhost:8000/auth-token/";
	var cred = { "username": username, "password": password	};
	
	request.post(link)
		.send(cred)
		.type('application/json')
		.end(function(err, res) {
			if(err) {
				var obj = JSON.parse(res.text);				
				_dispatchError(obj.non_field_errors);
			}
			else {
				var obj = JSON.parse(res.text);
				_dispatchLoginSuccess(username, obj.token);
			}
		});
  },
  logoutUser: function() {
    AppDispatcher.handleViewAction({
		type: Constants.USER_LOGGED_OUT,
		user: "",
		token: ""
	});
  },
};
