const Button = require('./Button');

module.exports = React.createClass({
  render: function() {
    return (
      <div className='actions'>
        <Button text="Open issue" action={this._open} />
        <Button text="Close issue" action={this._close} />
        <Button text="Reopen issue" action={this._reopen} />
      </div>
    );
  },
  _open: function() {
    console.log("Opened issue!");
  },
  _close: function() {
    console.log("Closed issue!");
  },
  _reopen: function() {
    console.log("Reopened issue!");
  }
});
