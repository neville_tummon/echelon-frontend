
module.exports = React.createClass({
  render: function() {
    var buttonStyle = {
      margin: 1
    };
    return (
      <button
       style={buttonStyle}
       className="btn btn-success"
       onClick={this.props.action}>
        {this.props.text}
       </button>
    )
  }
});
