const Post = require('./UserPost');
const Button = require('./Button');

function createPost(item) {
  return <Post data={item} />
}

module.exports = React.createClass({
  getInitialState: function() {
    return {expanded: false};
  },
  render: function() {
    return (
      <div className="comments">
        <Button text={this._getButtonText()} action={this._toggle} />
        {this._renderContents()}
      </div>
    );
  },
  _toggle: function() {
    this.setState({expanded: !this.state.expanded});
  },
  _getButtonText: function() {
    if (this.state.expanded === false) {
      return "Show Comments";
    }
    return "Hide Comments";
  },
  _renderContents: function() {
    if (this.state.expanded === false) {
      return null;
    }
    return (<div>
      {
        this.props.data.map(createPost)
      }
      <textarea rows="8" cols="40"></textarea>
      <button className="btn btn-default">Post</button>
    </div>)
  }
});
