const Summary = require('./Summary');
const ChangeHistory = require('./ChangeHistory');
const Comments = require('./Comments');
const DefectsApi = require('../Utils/DefectsApi');

module.exports = React.createClass({
  componentWillMount: function() {
    var state = this._getData(this.props.id);
    this.setState(state);
  },
  render: function() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <Summary data={this.state.detail} />
            <ChangeHistory data={this.state.history} />
            <Comments data={this.state.comments} />
          </div>
        </div>
      </div>
    );
  },
  _getData: function(id) {
    return {
      detail: DefectsApi.getDetail(id),
      history: DefectsApi.getChangeHistory(id),
      comments: DefectsApi.getComments(id)
    };
  },
});
