const Actions = require('../Actions/DefectActionCreators');
const Table = require('./DefectsTable');
const DefectListStore = require('../Stores/DefectListStore');
const DefectActions = require('../Actions/DefectActionCreators');
const SearchesStore = require('../Stores/SearchesStore');
const SearchControl = require('./SearchControl');
const TableNavControl = require('./TableNavControl');

module.exports = React.createClass({
  getInitialState: function() {
    return { 
      items: [],
      pageNr: 1,
      totalPages: 1,
      searchState: {
        searchParam: "",
        lastSearchParam: "",
        suggestions: []
      }
    }
  },
  componentWillMount: function() {
    DefectListStore.addChangeListener(this._onDefectListRequestCompleted);
    Actions.defectListRequested(1);
  },
  componentWillUnmount: function() {
    DefectListStore.removeChangeListener(this._onDefectListRequestCompleted);
  },
  render: function() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <SearchControl data={this.state.searchState}
                           onTextChanged={this._onTextChanged}
                           onSearchRequested={this._onSearchRequested} />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Table items={this.state.items} onItemClicked={this._onItemClicked}/>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3" />
          <div className="col-md-6">
            <TableNavControl pageNr={this.state.pageNr}
                             totalPages={this.state.totalPages} />
          </div>
          <div className="col-md-3" />
        </div>
      </div>
    )
  },
  _onItemClicked: function(id) {
    DefectActions.defectDetailsRequested(id);
  },
  _onDefectListRequestCompleted: function() {
    var data = DefectListStore.getData();
    data.searchState = SearchesStore.getData();
    this.setState(data);
  },
  _onTextChanged: function(e) {
    var data = this.state.searchState;
    data.searchParam = e.target.value;
    this.setState({searchState: data});
  },
  _onSearchRequested: function() {
    // Set state to loading.
    Actions.defectListRequested(1, this.state.searchState.searchParam);
  },
});
