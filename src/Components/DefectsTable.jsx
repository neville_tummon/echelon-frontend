const Link = require('react-router').Link;


module.exports = React.createClass({
  render: function() {
    return (
      <table id="search-results" className="table table-striped table-hover">
        <tbody>
          <tr>
            <th>Date created</th>
            <th>Status</th>
            <th>Project code</th>
            <th>Release ID</th>
            <th>Priority</th>
            <th>Reference</th>
            <th>Submitter</th>
          </tr>
          {this.props.items.map(this._createTableRow)}
        </tbody>
      </table>
    );
  },
  _createTableRow: function(item) {
    return <TableRow key={item.id}
                   data={item}
                   onClicked={this.props.onItemClicked} />
  },
});

var TableRow = React.createClass({
  render: function() {
    return (
      <tr onClick={this.handleClicked} style={this.getStyles()}>
        <td>{this.props.data.date_created}</td>
        <td>{this.props.data.status}</td>
        <td>{this.props.data.project_code}</td>
        <td>{this.props.data.release_id}</td>
        <td>{this.props.data.priority}</td>
        <td>
          <Link to="/issues/:id" params={{id: this.props.data.id}}>
            {this.props.data.reference}
          </Link>
        </td>
        <td>{this.props.data.submitter}</td>
      </tr>
    );
  },
  handleClicked: function() {
    this.props.onClicked(this.props.data.id)
  },
  getStyles: function() {
    return {
      color: this.getColorStyle(),
      fontWeight: this.getPriorityStyle(),
    };
  },
  getColorStyle: function() {
    if (this.props.data.status === "Open") {
      return "red";
    }
    return "grey";
  },
  getPriorityStyle: function() {
    if (this.isActiveImportantIssue()) {
      return "bold";
    }
    return "normal";
  },
  isActiveImportantIssue: function() {
    return this.props.data.priority === "High"
    && this.props.data.status == "Open";
  },
});
