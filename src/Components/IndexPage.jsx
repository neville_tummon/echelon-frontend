const React = require('react');
const Link = require('react-router').Link;

module.exports = React.createClass({
	render: function() {
		return(
			<div className="container-fluid">
				<div className="jumbotron">
					<h1>Welcome to BMM Echelon</h1>
					<p>A simple sentence describing the usefulness of Echelon</p>
					<Link to="issues">Call to action</Link>
				</div>
			</div>
		);
	},
});