const React = require('react');
var SessionActions = require('../Actions/SessionActionCreators');
var SessionStore = require('../Stores/SessionStore');

module.exports = React.createClass({
	getInitialState: function() {
		return {
			username: "",
			password: "",
			errors: []
		};
	},
	componentWillMount: function() {
		SessionStore.addChangeListener(this._onSessionChanged);
	},
	componentWillUnmount: function() {
		SessionStore.removeChangeListener(this._onSessionChanged);
	},
	render: function() {
		return (
			<div className="container">
				{this._renderForm()}
				{this._renderError()}
			</div>
		);
	},
	_isValidForm: function() {
		return this.state.username.length > 1 && this.state.password.length > 1;
	},
	_renderForm: function() {
		if (this.state.loggedIn) {
			return null;
		}
		return (
			<div className="login jumbotron center-block">
				<h1>Log in to Echelon</h1>
				<form role="form">
					<div className="form-group">
						<label htmlFor="username">Username</label>
						<input type="text" className="form-control" placeholder="Username"
								onChange={this._onUsernameChanged}
								value={this.state.username} />
					</div>
					<div className="form-group">
						<label htmlFor="password">Password</label>
						<input type="password" className="form-control" placeholder="Password"
								onChange={this._onPasswordChanged}
								value={this.state.password} />
					</div>
					{this._renderSubmitButton()}
				</form>
			</div>
		);
	},
	_renderSubmitButton: function() {
		return <button className="btn btn-danger" onClick={this._tryLogin}>Submit</button>
	},
	_renderError: function() {
		return (
			<ul>
				{this.state.errors.map((item, key) => <li key={key}>{item}</li>)}
			</ul>
		);
	},
	_onUsernameChanged: function(e) {
		this.setState({username: e.target.value});
	},
	_onPasswordChanged: function(e) {
		this.setState({password: e.target.value});
	},
	_tryLogin: function(e) {
		e.preventDefault();
		if (this._isValidForm()) {
			SessionActions.authenticateUser(this.state.username, this.state.password);
		}
	},
	_onSessionChanged: function() {
		var data = SessionStore.getData();
		
		this.setState({
			username: "",
			password: "",
			errors: data.errors,
			loggedIn: data.isUserAuthenticated
		});
		
		if (data.isUserAuthenticated) {
			// and perform a redirect
		}
	},
});