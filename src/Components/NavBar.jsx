const ReactRouter = require('react-router');
const Link = ReactRouter.Link;

var SessionActions = require('../Actions/SessionActionCreators');
var SessionStore = require('../Stores/SessionStore');

module.exports = React.createClass({
	getInitialState: function() {
		return {
			isUserAuthenticated: false,
			usernameLoggedIn: "",
		}
	},
	componentWillMount: function() {
		SessionStore.addChangeListener(this._onSessionChanged);
	},
	componentWillUnmount: function() {
		SessionStore.removeChangeListener(this._onSessionChanged);
	},
	render: function() {
		return (
			<nav className="navbar navbar-default navbar-fixed-top">
				<div className="container">
					<div className="navbar-header">
						<a className="navbar-brand" href="#">BMM Echelon</a>
					</div>
					<div className="navbar-collapse collapse">
						<ul className="nav navbar-nav">
							<li><Link to="/">Home</Link></li>
							<li><Link to="/issues">Issues</Link></li>
						</ul>
						{this._renderLogoutLink()}
					</div>
				</div>
			</nav>
		);
	},
	_renderLogoutLink: function() {
		if (!this.state.isUserAuthenticated) {
			return null;
		}
		return (
			<ul className="nav navbar-nav navbar-right">
				<li>{this._renderUserName()}</li>
				<li><Link to="/logout" onClick={this._onLogoutClicked}>Log Out</Link></li>
			</ul>
		);
		
	},
	_renderUserName: function() {
		return <Link>Logged in as {this.state.usernameLoggedIn}</Link>
	},
	_onSessionChanged: function() {
		this.setState(SessionStore.getData());
	},
	_onLogoutClicked: function() {
		SessionActions.logoutUser();
	}
});