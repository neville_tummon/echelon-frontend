const React = require('react');
const ReactRouter = require('react-router');
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const Redirect = ReactRouter.Redirect;

const Navbar = require('./NavBar');
const Detail = require('./DefectDetail');
const IssuesPage = require('./DefectsListPage');
const IndexPage = require('./IndexPage');
const LoginPage = require('./LoginPage');

var SessionStore = require('../Stores/SessionStore');

const App = React.createClass({
  render: function() {
    return (
		<div>
			<Navbar />
			<div className="content">
				{this.props.children}
			</div>
		</div>
	);
  }
});

var runAuthMiddleware = function(location, replaceWith) {
	var state = SessionStore.getData();
	if (!state.isUserAuthenticated) {
		replaceWith(null, "/login");
	}
}

const routes = (
	<Route path="/" component={App}>
		<Route path="login" component={LoginPage} />
		<Redirect from="/logout" to="/login"/>
		<IndexRoute onEnter={runAuthMiddleware} component={IndexPage} />
		<Route onEnter={runAuthMiddleware} path="issues" component={IssuesPage} />
		<Route onEnter={runAuthMiddleware} path="issues/:id" component={Detail} />
	</Route>
);

module.exports = routes;