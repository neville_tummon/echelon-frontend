module.exports = React.createClass({
  render: function() {
    var style = {
      marginTop: 3,
      marginBottom: 3,
      width: 400
    };
    return (
      <div style={style} className="input-group search">
        <input type="text" className="form-control" placeholder="Enter query..."
               onChange={this.props.onTextChanged}
               value={this.props.data.searchParam} />
        <span className="input-group-btn">
          <button className={this._getButtonClass()} type="button"
                  onClick={this._onSearchButtonClicked}>
            Search
          </button>
        </span>
      </div>
    );
  },
  _onSearchButtonClicked: function() {
    if (this._meetsMinimumInputSpec()) {
      this.props.onSearchRequested();
    }
  },
  _getButtonClass: function() {
    var classes = ["btn", "btn-danger"];
    if (!this._meetsMinimumInputSpec()) {
      classes.push("disabled");
    }
    return classes.join(" ");
  },
  _meetsMinimumInputSpec: function() {
    var data = this.props.data;
    return data.searchParam.length > 1
    & data.lastSearchParam != data.searchParam;
  }
});
