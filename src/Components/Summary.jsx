const ActionPanel = require('./ActionPanel');

module.exports = React.createClass({
  render: function() {
    return (
      <div className='desc'>
        <h1>Ticket ID: {this.props.data.id}</h1>
        <h2>{this.props.data.title} ({this.props.data.status})</h2>
        <ActionPanel />
        <p>{this.props.data.description}</p>
      </div>
    );
  }
});
