
module.exports = React.createClass({
  render: function() {
    return(
      <nav>
        <ul className="pager">
          {this._renderPreviousButton()}
          <li>{"Page " + this.props.pageNr + " of " + this.props.totalPages}</li>
          {this._renderNextButton()}        
        </ul>
      </nav>
    )
  },
  _renderPreviousButton: function() {
    classname = "previous";
    if (this.props.pageNr === 1) {
      classname += " disabled";
    }
    return (
      <li className={classname}>
        <a href="#">Previous page</a>
      </li>
    );
  },
  _renderNextButton: function() {
    classname = "next";
    if (this.props.pageNr === this.props.totalPages) {
      classname += " disabled";
    }
    return (
      <li className={classname}>
        <a href="#">Next page</a>
      </li>
    );
  },
});
