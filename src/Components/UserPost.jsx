module.exports = React.createClass({
  getInitialState: function() {
    return {hovered: false}
  },
  render: function() {
    return (
      <div className={this._getClasses()}
           onMouseOver={this._onMouseOver}
           onMouseOut={this._onMouseOut}>
        <h4>{this.props.data.user}</h4>
        <p>{this.props.data.comment}</p>
      </div>
    )
  },
  _onMouseOver: function() {
    this.setState({hovered: true});
  },
  _onMouseOut: function() {
    this.setState({hovered: false});
  },
  _getClasses: function() {
    var classes = ["post"]
    if (this.state.hovered) {
      classes.push("hover");
    }
    return classes.join(" ");
  },
});
