var assign = require('object-assign');
var AppDispatcher = require('../Dispatcher/AppDispatcher');
var BaseStore = require('./BaseStore');
var Constants = require('../Constants/Constants');

var _state = {
  items: [],
  pageNr: 1,
  totalPages: 1,
}

var DefectListStore = assign({}, BaseStore, {
  getData: function() {
    return _state;
  },
  dispatcherIndex: AppDispatcher.register(function(payload) {
    switch (payload.action.type) {
      case Constants.DEFECTLIST_REQUESTED:
        _state.items = payload.action.items;
        DefectListStore.emitChange();
        break;
      case Constants.DEFECT_SELECTED:
        break;
      default:
    }
  }),
});

module.exports = DefectListStore;
