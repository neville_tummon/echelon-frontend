var assign = require('object-assign');
var AppDispatcher = require('../Dispatcher/AppDispatcher');
var BaseStore = require('./BaseStore');
var Constants = require('../Constants/Constants');

var _searchState = {
  searchParam: "",
  lastSearchParam: "",
  suggestions: []
}

 var SearchesStore = assign({}, BaseStore, {
  getData: function() {
    return _searchState;
  },
  dispatcherIndex: AppDispatcher.register(function(payload) {
    switch (payload.action.type) {
      case Constants.DEFECTLIST_REQUESTED:
        _searchState.lastSearchParam = payload.action.query;
        SearchesStore.emitChange();
        break;
      default:
    }
  }),
});

module.exports = SearchesStore;
