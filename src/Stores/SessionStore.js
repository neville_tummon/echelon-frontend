var assign = require('object-assign');
var AppDispatcher = require('../Dispatcher/AppDispatcher');
var BaseStore = require('./BaseStore');
var Constants = require('../Constants/Constants');

var _state = {
  isUserAuthenticated: false,
  usernameLoggedIn: "",
  authToken: "",
  errors: []
}

var SessionStore = assign({}, BaseStore, {
  getData: function() {
    return _state;
  },
  dispatcherIndex: AppDispatcher.register(function(payload) {
    switch (payload.action.type) {
      case Constants.USER_LOGGED_IN:
        _state.isUserAuthenticated = true;
        _state.authToken = payload.action.token;
        _state.usernameLoggedIn = payload.action.user;
        _state.errors = [];
        SessionStore.emitChange();
        break;
      case Constants.AUTH_FAILED:
        _state.isUserAuthenticated = false;
        _state.authToken = "";
        _state.usernameLoggedIn = "";
        _state.errors = payload.action.errors;
        SessionStore.emitChange();
        break;
      case Constants.USER_LOGGED_OUT:
        _state.isUserAuthenticated = false;
        _state.authToken = "";
        _state.usernameLoggedIn = "";
        SessionStore.emitChange();
        break;
      default:
    }
  }),
});

module.exports = SessionStore;