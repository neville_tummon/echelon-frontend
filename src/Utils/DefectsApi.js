
module.exports = {
  getDetail: function(issueId) {
    return {
      id: issueId,
      status: 'Closed',
      title: 'Could not execute XYZ due to the Lorem',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    };
  },
  getChangeHistory: function(issueId) {
    return null;
  },
  getComments: function(issueId) {
    return [
      {user: 'Neville', comment: 'Did this work?'},
      {user: 'Other', comment: 'No not really.. Try this instead.'},
      {user: 'Neville', comment: 'I see. Actually this is not an issue. Please close it!'},
      {user: 'Other', comment: 'Alright then.'},
      {user: 'Neville', comment: 'Thanks!'},
    ];
  },
  getIssues: function(pageNr) {
    return [
      {id: 1, date_created: '21/07/2015', project_code: 'ABC.1234', release_id: 'XYZFF21A', priority: 'High', status: 'Open', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 2, date_created: '21/07/2015', project_code: 'ABC.1235', release_id: 'DSGDSG1C', priority: 'High', status: 'Open', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 3, date_created: '21/07/2015', project_code: 'ABC.1236', release_id: 'OTY47J6D', priority: 'Medium', status: 'Closed', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 4, date_created: '21/07/2015', project_code: 'ABC.1237', release_id: 'XYZFF21A', priority: 'High', status: 'Open', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 5, date_created: '21/07/2015', project_code: 'ABC.1238', release_id: 'XYZFF21A', priority: 'Low', status: 'Open', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 6, date_created: '21/07/2015', project_code: 'ABC.1239', release_id: 'XYZFF21A', priority: 'Observational', status: 'Closed', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
      {id: 7, date_created: '21/07/2015', project_code: 'ABC.1240', release_id: 'XYZFF21A', priority: 'High', status: 'Closed', reference: 'broken build when xyz', submitter: 'Neville Tummon'},
    ];
  }
};
