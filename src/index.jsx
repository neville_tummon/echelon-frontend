const React = require('react');
const routes = require('./Components/Routes');
const Router = require('react-router').Router;

React.render(<Router>{routes}</Router>, document.getElementById('content'));